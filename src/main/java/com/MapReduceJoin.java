package com;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.io.FileUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MapReduceJoin {
    public static class MapReduceJoinMapper  extends Mapper<LongWritable, Text,Text,Person> {


        @Override
        protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
            //获取文件名
            FileSplit split = (FileSplit) context.getInputSplit();
            String fileName = split.getPath().getName();
            //重读一下
            String s = value.toString();
            //按“，”号给s切分一下
            String[] lines = s.split(",");

            Person person = new Person();
            //根据后缀名判断一下 ，读取内容
            if (fileName.endsWith(".csv")){
                person.setName(lines[1]);
                person.setAge(Integer.parseInt(lines[2]));
                person.setUserId(lines[0]);
                person.setOrderId("NULL");
                person.setMingname(lines[3]);
                person.setTarget("1");
                context.write(new Text(lines[0]),person);

            }else if (fileName.endsWith(".dat")){
                person.setName("NULL");
                person.setAge(0);
                person.setUserId(lines[1]);
                person.setOrderId(lines[0]);
                person.setMingname("NULL");
                person.setTarget("2");
                context.write(new Text(lines[1]),person);
            }
        }
    }
     public static class MapReduceJoinReducer extends Reducer<Text,Person,Person, NullWritable>{
         @Override
         protected void reduce(Text key, Iterable<Person> values, Context context) throws IOException, InterruptedException {

             //new一个集合
             List<Person> orderlist = new ArrayList<>();
             String orderId = null;
             try {
                 for (Person person : values) {
                     if ("1".equals(person.getTarget())){
                         //new 一个对象                  //不能直接 list.add 所有要new
                         Person newperson = new Person();
                         //把person 复制给newperson
                         BeanUtils.copyProperties(newperson,person);
                         //然后把newperson 添加到order中
                         orderlist.add(newperson);
                     }else {
                         orderId = person.getOrderId();
                     }
                 }

                 for (Person person : orderlist) {
                     person.setOrderId(orderId);
                     context.write(person,NullWritable.get());
                 }
             }catch (Exception e){
                 e.printStackTrace();
             }
         }
     }

    public static void main(String[] args) throws IOException, ClassNotFoundException, InterruptedException {
        Configuration conf = new Configuration();

        Job job = Job.getInstance(conf);
        job.setJarByClass(MapReduceJoin.class);

        job.setMapperClass(MapReduceJoinMapper.class);
        job.setReducerClass(MapReduceJoinReducer.class);

        job.setNumReduceTasks(1);

        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(Person.class);

        job.setOutputKeyClass(Person.class);
        job.setOutputValueClass(NullWritable.class);
        if (new File("E:\\output").exists()) {
            FileUtils.deleteDirectory(new File("E:\\output"));
        }
        FileInputFormat.setInputPaths(job,new Path("E:\\wenjian"));
        FileOutputFormat.setOutputPath(job,new Path("E:\\output"));
        job.waitForCompletion(true);
    }
}
