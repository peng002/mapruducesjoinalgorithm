package com;

import org.apache.hadoop.io.Writable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

/*File 1:user.csv
  u001,senge,18,angelababy
  u002,laozhao,48,ruhua
  u003,xiaoxu,16,chunge
  u004,laoyang,28,zengge
  u005,nana,14,huangbo
 *
 * File 2 order.dat
 *order001,u001
  order002,u001
  order003,u005
  order004,u002
  order005,u003
  order006,u004
 *
 */

public class Person implements Writable {
    //订单id
    private String orderId;
    //用户id
    private String userId;
    //名字
    private String name;
    //年级
    private int age;
    //喜欢
    private String mingname;
    //标识  1 user.csv    2 order.dat
    private String Target;

    public Person(String orderId, String userId, String name, int age, String mingname, String target) {
        this.orderId = orderId;
        this.userId = userId;
        this.name = name;
        this.age = age;
        this.mingname = mingname;
        this.Target = target;
    }

    public Person() {
    }


    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getMingname() {
        return mingname;
    }

    public void setMingname(String mingname) {
        this.mingname = mingname;
    }

    public String getTarget() {
        return Target;
    }

    public void setTarget(String target) {
        Target = target;
    }

    @Override
    public void write(DataOutput dataOutput) throws IOException {
        dataOutput.writeUTF(orderId);
        dataOutput.writeUTF(userId);
        dataOutput.writeUTF(name);
        dataOutput.writeInt(age);
        dataOutput.writeUTF(mingname);
        dataOutput.writeUTF(Target);
    }
    @Override
    public void readFields(DataInput dataInput)throws IOException{
        this.orderId = dataInput.readUTF();
        this.userId = dataInput.readUTF();
        this.name = dataInput.readUTF();
        this.age = dataInput.readInt();
        this.mingname = dataInput.readUTF();
        this.Target = dataInput.readUTF();

    }
    @Override
    public String toString(){
        return this.orderId + ","+this.userId +","+this.name+","+this.age+","+this.mingname;
    }

}
